<?php

class User_Controller extends Base_Controller {

	public $restful = true;

	//Controllers for users
	public function post_user(){
		$user = new User;
		return Response::json($user->create_user(Input::json()));
	}
	public function put_user(){
		$user = new User;		
		return Response::json($user->update_user(Input::json()));
	}
	public function delete_user($num){
		$user = new User;
		return Response::json($user->delete_user($num));
	}
	public function get_users(){
		$user = new User;
		return Response::json($user->get_users(Input::all()));
	}


	//Controllers for tasks
	public function post_task(){
		$task = new MyTask;
		return Response::json($task->create_task(Input::json()));
	}
	public function put_task(){	
		$task = new MyTask;	
		return Response::json($task->update_task(Input::json()));
	}
	public function delete_task($num){
		$task = new MyTask;
		return Response::json($task->delete_task($num));
	}
	public function get_tasks(){
		$task = new MyTask;
		return Response::json($task->get_tasks(Input::all()));
	}

}