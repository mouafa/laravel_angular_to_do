<?Php
/////////////////////////////users////////////////////////

//users
Route::post('users',array('uses' => 'user@user'));
Route::put("users/(:num)",array('uses' => 'user@user'));
Route::get('users',array('uses' => 'user@users'));
Route::delete('users/(:num)',array('uses' => 'user@user'));

//Controllers for users
	public function post_user(){
		return Response::json(Users::create_user(Input::json()));
	}
	public function put_user(){		
		return Response::json(Users::update_user(Input::json()));
	}
	public function delete_user($num){
		return Response::json(Users::delete_user($num));
	}
	public function get_users(){
		return Response::json(Users::get_users(Input::all()));
	}


/////////////////////////////tasks////////////////////////

//tasks
Route::post('tasks',array('uses' => 'user@task'));
Route::put("tasks/(:num)",array('uses' => 'user@task'));
Route::get('tasks',array('uses' => 'user@tasks'));
Route::delete('tasks/(:num)',array('uses' => 'user@task'));

//Controllers for tasks
	public function post_task(){
		return Response::json(Tasks::create_task(Input::json()));
	}
	public function put_task(){		
		return Response::json(Tasks::update_task(Input::json()));
	}
	public function delete_task($num){
		return Response::json(Tasks::delete_task($num));
	}
	public function get_tasks(){
		return Response::json(Tasks::get_tasks(Input::all()));
	}

