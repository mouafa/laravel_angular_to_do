<?php

class Create_Users {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		// create the users table
		Schema::create('users', function($table) {
			$table->engine = 'InnoDB';
		    $table->increments('id');
		    $table->string('name', 128);		    
		    $table->string('email', 128)->nullable();;
		    $table->string('phone', 45)->nullable();
		    $table->date('date_of_birth')->nullable();
		    $table->blob('picture')->nullable();	    
		    $table->string('picture_file_name', 128)->nullable();
		    $table->string('user_name', 128);
		    $table->string('password', 128)->nullable();
		    $table->boolean('is_admin')->default(false);
		    $table->boolean('is_active')->default(true);
		    $table->integer('created_by')->unsigned();
		    $table->integer('updated_by')->unsigned();
		    $table->timestamps();		    
		});	

		// insert default user
		DB::table('users')->insert(array(
			'name' => 'Francis Miah',
			'email'	=> 'ras@gege.com',
            'phone' => '0244556699',
            'date_of_birth' => date('Y-m-d H:i:s'),
            'user_name' => 'admin',
            'password'	=> Hash::make('admin'),
            'is_admin'	=> true,
            'is_active'	=> true,
			'created_by' => 1,
			'updated_by' => 1,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		));
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('users');
	}

}